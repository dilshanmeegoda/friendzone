package com.friendz.friendzone;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;


public class NewTaskActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task);

        Spinner spinner = (Spinner) findViewById(R.id.frequency_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.frequency_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        Spinner deadLineSpinner = (Spinner) findViewById(R.id.deadLineType_spinner);
        ArrayAdapter<CharSequence> deadLineAdapter = ArrayAdapter.createFromResource(this,
                R.array.deadLineType_array, android.R.layout.simple_spinner_item);
        deadLineAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        deadLineSpinner.setAdapter(deadLineAdapter);

        Spinner taskSpinner = (Spinner) findViewById(R.id.task_spinner);
        ArrayAdapter<CharSequence> taskAdapter = ArrayAdapter.createFromResource(this,
                R.array.task_array, android.R.layout.simple_spinner_item);
        taskAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        taskSpinner.setAdapter(taskAdapter);

        Button friendPicker = (Button) findViewById(R.id.btnFriendPicker);
        friendPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent();
////                intent.setData(data);
//                intent.setClass(getApplication(), FriendPickerActivity.class);
//                startActivityForResult(intent, 0);
                Intent intent = new Intent(getApplication(),FriendPickerActivity.class);
                intent.setData(Uri.parse("picker://friend"));
                startActivity(intent);

            }
        });

//        createFragment(new SelectionFragment());

    }

//    private void createFragment(Fragment fr) {
//        FragmentManager manager = getSupportFragmentManager();
//        FragmentTransaction fragmentTransaction = manager.beginTransaction();
//        fragmentTransaction.replace(R.id.test, fr);
//        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
//        fragmentTransaction.addToBackStack("ddd");
//        fragmentTransaction.commit();
//    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.initiate_new_task, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

